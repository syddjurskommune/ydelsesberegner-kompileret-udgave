﻿app.controller("conditionsController", function ($scope, $log, $http, conditions) {
    $scope.conditions = conditions;
    $scope.questions = getQuestions();
    $scope.answers = [];

    if (conditions.length > 0) {
        $.each(conditions, function (index, condition) {
            if (condition.QuestionId != null) {
                getAnswers(index, condition);
            }
        });
    }

    $scope.add = function () {
        $scope.conditions.push({});
    };
    $scope.remove = function (index) {
        $scope.conditions.splice(index, 1);
    }
    $scope.questionChange = function (index, condition) {
        getAnswers(index, condition);
    }

    function getQuestions() {
        var applicationId = $("#ApplicationId").val();

        if (applicationId == undefined || applicationId.length === 0) return;

        $http.get("/api/QuestionsApi/GetQuestions?applicationId=" + applicationId).then(function (response) {
            if (response.status === 200) { //200 = OK
                $scope.questions = response.data;
            } else {
                $log.error(response);
            }
        }, function (error) {
            $log.error(error);
        });
    }
    function getAnswers(index, condition) {
        var questionId = condition.QuestionId;

        if (questionId == undefined) return;

        $http.get("/api/QuestionsApi/GetAnswers?questionId=" + questionId).then(function (response) {
            if (response.status === 200) { //200 = OK
                $scope.answers[index] = response.data;
            } else {
                $log.error(response);
            }
        }, function (error) {
            $log.error(error);
        });
    }
});