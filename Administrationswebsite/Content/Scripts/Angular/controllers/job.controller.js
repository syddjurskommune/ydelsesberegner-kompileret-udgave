﻿app.controller("jobController", function ($scope, job) {
    $scope.job = job;

    $scope.parseFloat = function (number) {
        if (number === undefined) return 0;
        return parseFloat(number);
    }
});