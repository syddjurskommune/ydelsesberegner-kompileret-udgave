﻿//jQuery DataTables - Configuration
var table = $("#benefit-types-table").DataTable({
    "pageLength": 25,
    "ajax": {
        "url": "/api/BenefitTypesApi/GetBenefitTypes",
        "dataSrc": "",
        "type": "GET",
        "data": function (d) {
            d.applicationId = getApplicationId();
        }
    },
    "columns": [
        { className: "details-control", orderable: false, defaultContent: "", width: "5%" },
        { data: "Name" },
        { data: "DisplayName" },
        { data: "MonthlyWorkHours", render: $.fn.dataTable.render.number(".", ",", 2) },
        { data: "MonthlyRate", render: $.fn.dataTable.render.number(".", ",", 2, "", " kr.") },
        { data: "HourlyRate", render: $.fn.dataTable.render.number(".", ",", 2, "", " kr.") },
        {
            render: function (data, type, row) {
                return "<a href=\"/BenefitTypes/Edit/" + row.BenefitTypeId + "\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-pencil\"></span></a> " +
                       "<a href=\"#\" data-benefit-type-id=\"" + row.BenefitTypeId + "\" class=\"btn btn-default btn-sm benefit-type-remove\"><span class=\"glyphicon glyphicon-trash\"></span></a>";
            },
            width: "10%"
        }
    ],
    "language": {
        "url": "/Content/Scripts/Libraries/datatables.danish.json"
    }
});

/* Formatting function for row details */
function format(d) {
    var conditions = d.Condition !== null && d.Condition.length > 0 ? getConditions(d.Condition) : "<tr><td>Der findes ingen betingelser for at få vist ydelsestypen</td></tr>";
    
    // `d` is the original data object for the row
    return "<div class=\"container\">" +
                "<ul class=\"nav nav-tabs\">" +
                    "<li class=\"active\"><a href=\"#conditions_" + d.SlideId + "\" data-toggle=\"tab\">Betingelser for at få vist ydelsestype</a></li>" +
                "</ul>" +
                "<div class=\"tab-content clearfix\">" +
                    "<div class=\"tab-pane active\" id=\"conditions_" + d.SlideId + "\">" +
                        "<table class=\"table\">" +
                            "<thead><tr>" +
                            "<th>Spørgsmål</th>" +
                            "<th>Svarmulighed</th>" +
                            "</tr></thead>" +
                            "<tbody>" + conditions + "</tbody>" +
                        "</table>" +
                    "</div>" +
                "</div>" +
             "</div>";
}

//Event listener for opening and closing details
$("#benefit-types-table tbody").on("click", "td.details-control", function () {
    var tr = $(this).closest("tr");
    var row = table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass("shown");
    }
    else {
        // Open this row
        row.child(format(row.data())).show();
        tr.addClass("shown");
    }
});

//Event listener for remove click
$("#benefit-types-table").on("click", "a.benefit-type-remove", function (e) {
    e.preventDefault();

    var benefitTypeId = $(this).data("benefit-type-id");

    console.log("remove", benefitTypeId);

    deleteBenefitType(benefitTypeId);

    table.ajax.reload(null, false);
});

//Event listener for application change
$("#Applications").change(function () {
    table.ajax.reload(null, false);
});

//Event listener for Create Benefit Type click
$("#create-benefit-type").click(function () {
    var appId = $("#Applications").val();
    var url = "/BenefitTypes/Create";

    window.location.href = url + "?applicationId=" + appId;
});

//Helper methods
function getApplicationId() {
    return $("#Applications").val().length === 0 ? 1 : $("#Applications").val();
}

function getConditions(conditions) {
    var data = "";

    $.each(conditions, function (index, object) {
        data += "<tr><td>" + object.Question.QuestionSystemName + "</td><td>" + object.Answer.AnswerText + "</td></tr>";
    });

    return data;
}

function deleteBenefitType(benefitTypeId) {
    jQuery.ajax({
        type: "DELETE",
        url: "/api/BenefitTypesApi/DeleteBenefitType?id=" + benefitTypeId,
        async: false,
        success: function (response) {
            //console.log("delete benefit type success", response);
        },
        error: function (response) {
            console.log("Der opstod en fejl under sletning af ydelsestypen.", response);
        }
    });
}